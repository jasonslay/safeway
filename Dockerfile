FROM jasonslay/python-selenium

ADD requirements.txt requirements.txt
ADD safeway.py safeway.py
RUN pip install --upgrade pip wheel setuptools
RUN pip install --upgrade -r requirements.txt

ENTRYPOINT ["python", "./safeway.py"]
