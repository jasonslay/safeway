import argparse
import os
import sys
import time
import requests
import logging
import traceback

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException, NoSuchElementException


logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format="%(asctime)s %(levelname)s %(message)s",
)


def send_pushover_message(title, message, image=None):
    app_token = os.environ.get("PUSHOVER_APP_TOKEN")
    user_key = os.environ.get("PUSHOVER_USER_KEY")
    if app_token and user_key:
        if image:
            files = {
                "attachment": (os.path.basename(image), open(image, "rb"), "image/png")
            }
        else:
            files = {}
        requests.post(
            "https://api.pushover.net/1/messages.json",
            data={
                "token": app_token,
                "user": user_key,
                "title": title,
                "message": message,
            },
            files=files,
        )


def clip_coupons(username, password, debug=False):
    try:
        chrome_options = webdriver.ChromeOptions()
        if not debug:
            chrome_options.add_argument("headless")
        chrome_options.add_argument("no-sandbox")
        chrome_options.add_argument('lang=en-US')
        chrome_options.add_argument("window-size=1920x1080")
        if (proxy_server_uri := os.environ.get("PROXY_SERVER_URI")):
            chrome_options.add_argument(f"--proxy-server={proxy_server_uri}")
        browser = webdriver.Chrome(
            "chromedriver", options=chrome_options 
        )
        browser.get("https://www.safeway.com/justforu/coupons-deals.html")
        email_input = browser.find_element_by_name("userId")
        email_input.send_keys(username)
        password_input = browser.find_element_by_name("inputPassword")
        password_input.send_keys(password)
        browser.find_element_by_id("btnSignIn").click()
        WebDriverWait(browser, 10).until(
            EC.visibility_of_element_located((By.CLASS_NAME, "coupon-grid-container"))
        )
        try:
            browser.find_element_by_link_text('Close').click()
        except NoSuchElementException:
            pass
        scroll_to_show_all_coupons(browser)
        clicked = add_coupons(browser)

    except Exception:
        logging.error(traceback.format_exc())
        browser.get_screenshot_as_file("/tmp/safeway.png")
        send_pushover_message(
            message=traceback.format_exc(), title=f"Error! ({username})", image="/tmp/safeway.png"
        )
    else:
        logging.info("Added {} new coupons".format(clicked))
        if clicked:
            send_pushover_message(
                message="Added {} new coupons".format(clicked), title=f"Added coupons! ({username})"
            )
    finally:
        browser.quit()


def scroll_to_show_all_coupons(browser):
    logging.info("Scrolling...")
    try:
        while True:
            WebDriverWait(browser, 3).until(
                EC.visibility_of_element_located((By.CLASS_NAME, "load-more"))
            )
            more_button = browser.find_element_by_class_name("load-more")
            browser.execute_script('return arguments[0].scrollIntoView({behavior: "auto", block: "center", inline: "center"});', more_button)
            time.sleep(0.25)
            more_button.click()
            time.sleep(0.25)
    except TimeoutException:
        pass


def add_coupons(browser):
    logging.info("Adding coupons...")
    clicked = 0
    coupons = [c for c in browser.find_elements_by_xpath("//button[contains(text(),'Clip Coupon')]") if c.text == "Clip Coupon"]
    for coupon in coupons:
        browser.execute_script('return arguments[0].scrollIntoView({behavior: "auto", block: "center", inline: "center"});', coupon)
        coupon.click()
        clicked += 1
    return clicked


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Add Safeway coupons')
    parser.add_argument('username', help='Safeway.com username')
    parser.add_argument('password', help='Safeway.com password')
    parser.add_argument('--debug', action='store_true', help='debug with an interactive Chrome session')
    args = parser.parse_args()
    clip_coupons(username=args.username, password=args.password, debug=args.debug)
